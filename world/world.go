package world

import (
	"fmt"

	"gitlab.com/C1ARKGABLE/ants/ant"
)

func World() {
	// Get a greeting message and print it.
	message := ant.Hello("Gladys")
	fmt.Println(message)
}
