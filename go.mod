module gitlab.com/C1ARKGABLE/ants

go 1.16

replace gitlab.com/C1ARKGABLE/ants/world => ./world

replace gitlab.com/C1ARKGABLE/ants/ant => ./ant

require gitlab.com/C1ARKGABLE/ants/world v0.0.0-20210405003915-adec2c85d513
